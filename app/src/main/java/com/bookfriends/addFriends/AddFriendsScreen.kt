package com.bookfriends.addFriends

import android.content.Context
import android.net.ConnectivityManager
import android.net.Uri
import android.view.WindowInsets.Side
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.compose.rememberImagePainter
import com.bookfriends.home.social_pink
import com.bookfriends.models.User
import com.bookfriends.network.Resources
import com.bookfriends.ui.theme.BooksTheme
import kotlinx.coroutines.launch
import kotlin.concurrent.thread

@OptIn(androidx.compose.animation.ExperimentalAnimationApi::class)
@Composable
fun AddFriendsScreen(
    addFriendsViewModel: AddFriendsViewModel?,
) {
    connectionChecker(addFriendsViewModel, LocalContext.current)
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun connectionChecker(
    addFriendsViewModel: AddFriendsViewModel?,
    context: Context,
) {
    // on below line creating a variable
    // for connection status.
    val connectionType = remember {
        mutableStateOf("Not Connected")
    }
    // on below line we are creating a column,
    Column(
        // on below line we are adding
        // a modifier to it,
        modifier = Modifier
            .fillMaxSize()
            // on below line we are
            // adding a padding.
            .padding(all = 10.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {

        // A Thread that will continuously
        // monitor the Connection Type
        Thread(Runnable {
            while (true) {

                // Invoking the Connectivity Manager
                val cm =
                    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

                // Fetching the Network Information
                val netInfo = cm.allNetworkInfo

                // on below line finding if connection
                // type is wifi or mobile data.
                for (ni in netInfo) {
                    if (ni.typeName.equals("WIFI", ignoreCase = true))
                        if (ni.isConnected) connectionType.value = "WIFI"
                    if (ni.typeName.equals("MOBILE", ignoreCase = true))
                        if (ni.isConnected) connectionType.value = "MOBILE DATA"
                    if (ni.typeName.equals("NOT CONNECTED", ignoreCase = true))
                        if (!ni.isConnected) connectionType.value = "NOT CONNECTED"
                }
            }
        }).start() // Starting the thread


        // on below line we are adding a text for heading.
        Text(
            // on below line we are specifying text
            text = "Internet Connectivity Checker",
            // on below line we are specifying text color,
            // font size and font weight
            color = social_pink,
            fontSize = 20.sp,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center
        )

        // on below line adding a spacer.
        Spacer(modifier = Modifier.height(15.dp))

        // on below line we are adding a text
        // for displaying connection type.
        Text(
            // on below line we are specifying text
            text = connectionType.value,
            // on below line we are specifying text color,
            // font size and font weight
            color = social_pink, fontSize = 15.sp, fontWeight = FontWeight.Bold
        )

        if (connectionType.value.equals("Not Connected", ignoreCase = true)) {
            Text(
                // on below line we are specifying text
                text = "You need to be connected to a network to use this functionality.",
                // on below line we are specifying text color,
                // font size and font weight
                color = social_pink, fontSize = 15.sp, fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center
            )
        } else {
            // Add friends
            val addFriendsUiState = addFriendsViewModel?.addFriendsUiState ?: AddFriendsUiState()

            val scaffoldState = rememberScaffoldState()

            SideEffect {
                addFriendsViewModel?.loadUsers()
            }

            Scaffold(
                scaffoldState = scaffoldState,
            ) { padding ->
                Column(
                    modifier = Modifier.padding(padding),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    when (addFriendsUiState.UserList) {
                        is Resources.Loading<*> -> {
                            CircularProgressIndicator(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .wrapContentSize(align = Alignment.Center)
                            )
                        }
                        is Resources.Success<*> -> {
                            Text(
                                text = "Order by",
                                style = MaterialTheme.typography.h5,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier.padding(8.dp)
                            )
                            // Get shared preferences
                            val prefs = LocalContext.current.getSharedPreferences(
                                "com.bookfriends.prefs",
                                Context.MODE_PRIVATE
                            )
                            // Get the value of the key "sort_by"
                            val sortBy = prefs.getString("sort_by", "First Name")

                            // Get the value of the key "sort_order"
                            val sortOrder = prefs.getString("sort_order", "Ascending")

                            if (sortBy != null) {
                                addFriendsViewModel?.onOptionSelected(sortBy)
                            }

                            val radioOptions = addFriendsUiState.radioOptions

                            Column {
                                Column(
                                    modifier = Modifier.fillMaxWidth(),
                                ) {
                                    radioOptions.forEach { item ->
                                        Row(
                                            verticalAlignment = Alignment.CenterVertically
                                        ) {
                                            RadioButton(
                                                selected = addFriendsUiState.selectedRadioOption == item,
                                                onClick = {
                                                    addFriendsViewModel?.onOptionSelected(item)
                                                    // Save the value of the key "sort_by"
                                                    prefs.edit().putString("sort_by", item).apply()
                                                },
                                                enabled = true,
                                            )
                                            Text(
                                                text = item,
                                                modifier = Modifier.padding(start = 8.dp)
                                            )
                                        }
                                    }
                                }
                            }

                            val radioOptionsOrder = addFriendsUiState.radioOptionsOrder

                            if (sortOrder != null) {
                                addFriendsViewModel?.onOptionOrderSelected(sortOrder)
                            }

                            Column {
                                Column(
                                    modifier = Modifier.fillMaxWidth(),
                                ) {
                                    radioOptionsOrder.forEach { item ->
                                        Row(
                                            verticalAlignment = Alignment.CenterVertically
                                        ) {
                                            RadioButton(
                                                selected = addFriendsUiState.selectedRadioOptionOrder == item,
                                                onClick = {
                                                    addFriendsViewModel?.onOptionOrderSelected(item)
                                                    // Save the value of the key "sort_order"
                                                    prefs.edit().putString("sort_order", item)
                                                        .apply()
                                                },
                                                enabled = true,
                                            )
                                            Text(
                                                text = item,
                                                modifier = Modifier.padding(start = 8.dp)
                                            )
                                        }
                                    }
                                }
                            }

                            LazyVerticalGrid(
                                cells = GridCells.Adaptive(300.dp),
                                contentPadding = PaddingValues(
                                    start = 12.dp,
                                    top = 16.dp,
                                    end = 12.dp,
                                    bottom = 16.dp
                                )
                            ) {
                                if (addFriendsUiState.UserList.data?.size == 0) {
                                    item {
                                        Text(
                                            text = "No users yet",
                                            style = MaterialTheme.typography.h6,
                                            fontWeight = FontWeight.Bold,
                                            modifier = Modifier
                                                .fillMaxWidth()
                                                .wrapContentHeight()
                                                .padding(16.dp)
                                        )
                                    }
                                } else {
                                    items(
                                        addFriendsUiState.UserList.data ?: listOf()
                                    ) { User ->
                                        UserItem(
                                            User = User
                                        )
                                    }
                                }
                            }
                        }
                        else -> {
                            Text(
                                text = addFriendsUiState
                                    .UserList.throwable?.localizedMessage ?: "Error desconocido",
                                color = Color.Red
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun UserItem(
    User: User,
) {
    val imageUri = rememberSaveable { mutableStateOf("") }
    val painter = rememberImagePainter(imageUri.value.ifEmpty { com.bookfriends.R.drawable.ic_user }
    )

    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent()
    ) { uri: Uri? ->
        uri?.let { imageUri.value = it.toString() }
    }

    Card(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        elevation = 8.dp
    ) {

        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                modifier = Modifier.fillMaxWidth()
            ) {
                Card(
                    shape = CircleShape,
                    modifier = Modifier
                        .size(20.dp)
                ) {
                    CompositionLocalProvider(
                        LocalContentAlpha provides ContentAlpha.disabled,
                    ) {
                        AsyncImage(model = "${User.imageUrl}",
                            modifier = Modifier
                                .clip(RoundedCornerShape(15.dp))
                                .wrapContentSize()
                                .border(color = Color.LightGray, width = 1.dp)
                                .clickable { launcher.launch("${User.imageUrl}") }
                                .align(CenterVertically),
                            contentScale = ContentScale.Crop,
                            contentDescription = "Profile picture")
                    }
                }
                Spacer(modifier = Modifier.width(8.dp))

                Text(
                    text = "${User.firstName} ${User.lastName}",
                    style = MaterialTheme.typography.h6,
                    fontWeight = FontWeight.Bold,
                    maxLines = 1,
                    overflow = TextOverflow.Clip,
                    modifier = Modifier.padding(4.dp)
                )
            }
        }
    }
}

@Preview
@Composable
fun PrevAddFriendsScreen() {
    BooksTheme {
        AddFriendsScreen(
            addFriendsViewModel = null
        )
    }
}


